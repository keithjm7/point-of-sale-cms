'use strict';

angular
    .module('pointofsale', ['simplePagination', 'angularTreeview', 'ngAnimate', 'ngFileUpload', 'ui.select', 'colorpicker.module',
        'ngResource', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'toastr', 'ngDialog', 'ngTable', 'ngCookies', 'angular-loading-bar',
        'ngOnlyNumberApp', 'ngMessages', 'textAngular', 'oc.lazyLoad', 'jdf.ngThemeSwitcher', 'ncy-angular-breadcrumb', 'ui.nested.combobox', 'ngActivityIndicator',
        'angular-nicescroll', 'toggle-switch', 'angularMoment', 'chart.js', 'validation', 'validation.rule', 'ivh.treeview'
    ])


// .constant('API_URL', 'http://localhost:3000/api/1.0')
// .constant('photoapi', 'http://localhost:3000/')
// .constant('API_URL', 'http://52.64.27.145:5001/api/1.0')
// .constant('photoapi', 'http://52.64.27.145:5001/')

    .constant('API_URL', 'http://localhost:3000/api/1.0')
    .constant('photoapi', 'http://localhost:3000/')
    .constant('baseURL', 'http://localhost:3000/')
    // .constant('API_URL', 'http://52.64.27.145:5001/api/1.0')
    // .constant('photoapi', 'http://52.64.27.145:5001/')
    /*.constant('API_URL', 'http://192.168.1.28:3000/api/1.0')
    .constant('photoapi', 'http://192.168.1.28:3000/')*/



.config(function($httpProvider, ngDialogProvider, $activityIndicatorProvider, $ocLazyLoadProvider, ivhTreeviewOptionsProvider) {
    $httpProvider.interceptors.push('authInterceptor');

    $activityIndicatorProvider.setActivityIndicatorStyle('SpinnerDark');

    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false
    });

    $ocLazyLoadProvider.config({
        debug: false,
        events: true
    });
    ivhTreeviewOptionsProvider.set({
        defaultSelectedState: false,
        validate: true,
        twistieExpandedTpl: '<span class="arrow-down"></span>',
        twistieCollapsedTpl: '<span class="arrow-right"></span>',
        twistieLeafTpl: '&nbsp;&nbsp;',
    });
})

.run(function($rootScope, $window, toastr) {
    $rootScope.prodID = '';
    $rootScope.online = navigator.onLine;


    $rootScope.$on('$stateChangeStart', function(event, toState) {
        if (toState.name !== 'auth.login') {
            $rootScope.module = toState.data.module;
            $rootScope.selectedTab = toState.data.tab;
        }
    });


    $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
            $rootScope.online = false;
            toastr.warning('Network State: OFFLINE', 'Information');
        });
    }, false);
    $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
            $rootScope.online = true;
            toastr.warning('Network State: ONLINE', 'Information');
        });
    }, false);

});
