'use strict';
angular.module('pointofsale')
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('main.settingsStore', {
                url: '/store',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/stores/store.html',
                        controller: 'ACStoresCTRL'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Stores',
                    parent: 'main.home'
                },
                data: {
                    module: 'Stores',
                    tab: 6
                }
            })
            .state('main.settingsAddStore', {
                url: '/store/info/:data',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/stores/storedetail.html',
                        controller: 'ACADDStoreCTRL'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Store Information',
                    parent: 'main.settingsStore'
                },
                data: {
                    module: 'Store Information',
                    tab: 6
                }
            })
            .state('main.settingsAttribute', {
                url: '/Attribute',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/attributes/attribute.html',
                        controller: 'settingsAttributes'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Attributes',
                    parent: 'main.home'
                },
                data: {
                    module: 'Attributes',
                    tab: 8
                }
            })
            .state('main.settingsAttributeADD', {
                url: '/Attribute',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/attributes/attributedetail.html',
                        controller: 'settingsAttribute'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Attribute Detail',
                    parent: 'main.settingsAttribute'
                },
                data: {
                    module: 'Attribute Detail',
                    tab: 8
                }
            })
            .state('main.settingsBanks', {
                url: '/banks',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/banks/banks.html',
                        controller: 'banksCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Banks',
                    parent: 'main.home'
                },
                data: {
                    module: 'Banks',
                    tab: 9
                }
            })
            .state('main.settingsBank', {
                url: '/bank/:data',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/banks/bank.html',
                        controller: 'bankCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Bank Detail',
                    parent: 'main.settingsBanks'
                },
                data: {
                    module: 'Banks',
                    tab: 9
                }
            })
            .state('main.settingsCategories', {
                url: '/categories',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/category/categories.html',
                        controller: 'categoriesCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Category',
                    parent: 'main.home'
                },
                data: {
                    module: 'Category',
                    tab: 9
                }
            })
            .state('main.settingsCategory', {
                url: '/category/:data',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/category/category.html',
                        controller: 'ACCatCTRL'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Category Detail',
                    parent: 'main.settingsCategories'
                },
                data: {
                    module: 'Category',
                    tab: 9
                }
            })
            .state('main.settingsProducttype', {
                url: '/ProductType',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/productType/prodtype.html',
                        controller: 'ACPTypesCTRL'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Product Type',
                    parent: 'main.home',
                },
                data: {
                    module: 'Product Type',
                    tab: 10
                }
            })
            .state('main.settingsAddProducttype', {
                url: '/ProductType/:data',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/productType/prodtypedetail.html',
                        controller: 'ACPTypeCTRL'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Product Type Detail',
                    parent: 'main.settingsProducttype'
                },
                data: {
                    module: 'Product Type Detail',
                    tab: 10
                }
            })
            .state('main.suppliers', {
                url: '/suppliers',
                params: {
                    data: null
                },
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/suppliers/suppliers.html',
                        controller: 'supplierCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Suppliers',
                    parent: 'main.home'
                },
                data: {
                    module: 'Suppliers',
                    tab: 11
                }
            })
            .state('main.employees', {
                url: '/employees',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/employees/employees.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Employees',
                    parent: 'main.home'
                },
                data: {
                    module: 'Employees',
                    tab: 12
                }
            });
    });
