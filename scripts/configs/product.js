'use strict';
angular.module('pointofsale')
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $validationProvider) {
        $stateProvider
            .state('main.viewProduct', {
                url: '/viewProduct',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/product/ProdCatalogue.html',
                        controller: 'productCatalogueCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Products',
                    parent: 'main.home'
                },
                data: {
                    module: 'Products',
                    tab: 1
                }
            })
            .state('main.addproduct', {
                url: '/addproduct/:product_id',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/product/addproduct.html',
                        controller: 'supplierProductCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Create New',
                    parent: 'main.viewProduct'
                },
                data: {
                    module: 'Create New',
                    tab: 1
                }
            })
            .state('main.association', {
                url: '/association/:product_id',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/product/assoc.html',
                        controller: 'supplierProductCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Associations',
                    parent: 'main.viewProduct'
                },
                data: {
                    module: 'Associations',
                    tab: 1
                }
            });
    });
