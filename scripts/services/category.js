'use strict';

angular.module('pointofsale')
    .factory('categoryFactory', function($http, API_URL) {
        return {
            getAllCategory: function() {
                return $http({
                    url: API_URL + '/categories',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                   
                });
            },
            getCategory: function(id) {
                return $http({
                    url: API_URL + '/categories/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteCategory: function(id) {
                return $http({
                    url: API_URL + '/categories/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveCategory: function(data) {
                return $http({
                    url: API_URL + '/categories',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateCategory: function(id, data) {
                return $http({
                    url: API_URL + '/categories/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
