/*jslint camelcase:false*/
'use strict';

angular.module('pointofsale')
    .factory('_', function($window) {
        return $window._;
    })
    .factory('async', function($window) {
        return $window.async;
    })
    .factory('moment', function($window) {
        return $window.moment;
    })
    .factory('authInterceptor', function($q, $window, $location) {
        return {
            // Add authorization token to headers
            request: function(config) {
                config.headers = config.headers || {};
                if ($window.localStorage.userToken) {
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.userToken;
                }
                return config;
            },

            // Intercept 401s and redirect you to login
            responseError: function(response) {
                if (response.status === 401) {

                    $window.localStorage.userToken = '';
                    $window.localStorage.user = '';

                    $location.path('/login');

                    return $q.reject(response);
                } else if (response.status === 500) {
                    // $location.path('/login');
                    return $q.reject(response);
                } else if (response.status === 0) {
                    // $location.path('/login');
                    return $q.reject(response);
                } else {
                    return $q.reject(response);
                }
            }
        };
    });
