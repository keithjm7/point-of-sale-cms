'use strict';

angular.module('pointofsale')
    .factory('productFactory', function($http, API_URL,toastr) {
        return {
            getAllProduct: function() {
                return $http({
                    url: API_URL + '/product',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getAllPublish: function() {
                return $http({
                    url: API_URL + '/products/publish',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getAllUnpublish: function() {
                return $http({
                    url: API_URL + '/products/unpublish',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getAllArchived: function() {
                return $http({
                    url: API_URL + '/products/archive',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getAllActive: function() {
                return $http({
                    url: API_URL + '/products/active',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getSearch: function(productName) {
                return $http({
                    url: API_URL + '/searchProduct/' + productName,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getProduct: function(id) {
                return $http({
                    url: API_URL + '/product/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return (res.data);
                });
            },
            deleteProduct: function(id) {
                return $http({
                    url: API_URL + '/product/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveProduct: function(data) {
                return $http({
                    url: API_URL + '/product',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                }, function(err) {
                    toastr.error(err.data.response.msg, 'Error');
                    return;
                });
            },
            updateProduct: function(id, data) {
                return $http({
                    url: API_URL + '/product/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            },
            activeArchive: function(id, data) {
                return $http({
                    url: API_URL + '/products/'+ id +'/archive',
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            },
            publishUnpublish: function(id, data) {
                return $http({
                    url: API_URL + '/products/' + id + '/publish',
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
