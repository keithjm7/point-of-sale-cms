'use strict';

angular.module('pointofsale')
    .factory('supplierFctry', function($http, API_URL) {
        return {
            getAllSuppliers: function() {
                return $http({
                    url: API_URL + '/supplier',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getSupplier: function(id, cb) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteSupplier: function(id, cb) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });

                
            },
            saveSupplier: function(data) {
                console.log(data);
                return $http({
                    url: API_URL + '/supplier',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });

                
            },
            updateSupplier: function(id, data) {
                return $http({
                    url: API_URL + '/supplier/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
