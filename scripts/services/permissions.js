'use strict';

angular.module('pointofsale')
    .factory('useraccountsFctry', function($http, API_URL) {
        return {
            generatePasswordString: function(length) {
                var text = '';
                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

                for (var i = 0; i < length; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }

                return text;
            },
            getAllUserAccounts: function() {
                return $http({
                    url: API_URL + '/useraccounts',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveUserAccount: function(data) {
                console.log('saveUserAccount: ',data);
                return $http({
                    url: API_URL + '/useraccounts',
                    method: 'POST',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            },
            getUserAccount: function(id) {
                return $http({
                    url: API_URL + '/useraccounts/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateUserAccount: function(id, data) {
                return $http({
                    url: API_URL + '/useraccounts/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteUserAccount: function(id) {
                return $http({
                    url: API_URL + '/useraccounts/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    })
    .factory('usergroupsFctry', function($http, API_URL) {
        return {
            createUserGroup: function(data) {
                return $http({
                    url: API_URL + '/usergroups',
                    method: 'POST',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            },
            getAllUserGroups: function() {
                return $http({
                    url: API_URL + '/usergroups',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getUserGroupById: function(id) {
                return $http({
                    url: API_URL + '/usergroups/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteUserGroup: function(id) {
                return $http({
                    url: API_URL + '/usergroups/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },

            updateUserGroup: function(id, data) {
                return $http({
                    url: API_URL + '/usergroups/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    })
    .factory('permissionFctry', function($http, API_URL) {
        return {
            getAllModulesPermissions: function() {
                return $http({
                    url: API_URL + '/modules/permissions',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            generateAccessPermission: function(roleid) {
                return $http({
                    url: API_URL + '/useraccounts/' + roleid + '/permissions',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            setGroupPrivilege : function(roleid,data){
                return $http({
                    url: API_URL + '/useraccounts/' + roleid + '/permissions',
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
