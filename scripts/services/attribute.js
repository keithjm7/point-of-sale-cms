'use strict';

angular.module('pointofsale')
    .factory('attributeFactory', function($http, API_URL) {
        return {
            getAllAttribute: function() {
                return $http({
                    url: API_URL + '/attributes',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getAttribute: function(id) {
                return $http({
                    url: API_URL + '/attributes/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteAttribute: function(id) {
                return $http({
                    url: API_URL + '/attributes/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveAttribute: function(data) {
                return $http({
                    url: API_URL + '/attributes',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateAttribute: function(id, data) {
                return $http({
                    url: API_URL + '/attributes/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
