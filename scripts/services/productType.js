'use strict';
angular.module('pointofsale')
    .factory('ptypeFactory', function($http, API_URL, Upload) {
        return {
            getAllProdType: function() {
                return $http({
                    url: API_URL + '/productType',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getProdType: function(id) {
                return $http({
                    url: API_URL + '/producttype/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                    
                });
            },
            deleteProdType: function(id, cb) {
                return $http({
                    url: API_URL + '/productType/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    cb(res.data);
                });

            },
            saveProdType: function(data) {
                console.log(data);
                return $http({
                    url: API_URL + '/productType',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateProdType: function(id, data) {
                return $http({
                    url: API_URL + '/productType/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
