'use strict';

angular.module('pointofsale')
    .factory('priceHistoryFactory', function($http, API_URL) {
        return {
            getPriceHistory: function(id) {
                return $http({
                    url: API_URL + '/pricehistory/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getStoreP: function(id) {
                return $http({
                    url: API_URL + '/storeowner/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                    
                });
            },
            deleteSP: function(id) {
                return $http({
                    url: API_URL + '/pricehistory/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveSP: function(id,data) {
                return $http({
                    url: API_URL + '/pricehistory/' + id,
                    data: data,
                    method: 'PUT',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateSP: function(id, data) {
                return $http({
                    url: API_URL + '/pricehistory/' + id + '/detail',
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
