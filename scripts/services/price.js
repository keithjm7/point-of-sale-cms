'use strict';

angular.module('pointofsale')
    .factory('priceFactory', function($http, API_URL) {
        return {
            getAllPrice: function() {
                return $http({
                    url: API_URL + '/priceHistory',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getPrice: function(id) {
                return $http({
                    url: API_URL + '/priceHistory/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deletePrice: function(id) {
                return $http({
                    url: API_URL + '/priceHistory/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            savePrice: function(data) {
                return $http({
                    url: API_URL + '/priceHistory',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updatePrice: function(id, data) {
           
                return $http({
                    url: API_URL + '/priceHistory/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
