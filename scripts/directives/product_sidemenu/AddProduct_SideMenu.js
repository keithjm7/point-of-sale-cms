'use strict';

angular.module('pointofsale')
    .directive('addprodSidemenu', function() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/directives/product_sidemenu/addProduct_SideMenu.html'
        };
    });