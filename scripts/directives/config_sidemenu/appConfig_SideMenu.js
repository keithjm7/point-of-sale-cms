'use strict';

angular.module('pointofsale')
    .directive('appconfigSidemenu', function() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/directives/config_sidemenu/appConfig_SideMenu.html'
        };
    });