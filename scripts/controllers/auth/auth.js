'use strict';
angular.module('pointofsale')
    .controller('AuthCtrl', function($scope,$state) {
        $scope.submit = function() {
            $state.go('main.home');
        };
    });
