 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('financialCtrl', function($scope, $filter, $window, toastr) {
         $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
         $scope.series = ['Series A', 'Series B'];

         $scope.data = [
             [65, 59, 80, 81, 56, 55, 40],
             [28, 48, 40, 19, 86, 27, 90]
         ];

         $scope.lineChartslabels = ["January", "February", "March", "April", "May", "June", "July"];
         $scope.lineChartsseries = ['Series A', 'Series B'];
         $scope.lineChartdata = [
             [65, 59, 80, 81, 56, 55, 40],
             [28, 48, 40, 19, 86, 27, 90]
         ];

         $scope.radarlabels = ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"];
         $scope.radardata = [
             [65, 59, 90, 81, 56, 55, 40],
             [28, 48, 40, 19, 96, 27, 100]
         ];

         $scope.donutlabels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
         $scope.donutdata = [300, 500, 100];
     });
