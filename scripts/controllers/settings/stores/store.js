/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('ACADDStoreCTRL', function(_, $scope, $window, $state, $stateParams, $timeout, toastr, $activityIndicator, storeFactory) {
        $scope.store = {};
        
        if (_.isEmpty($stateParams.data) || _.isUndefined($stateParams.data)) {
            $scope.isNew = true;
            $scope.storesave = {};
        } else {
            console.log('else');
            $activityIndicator.startAnimating();
            $scope.isNew = false;
            storeFactory.getStore($stateParams.data).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    if (data.response && _.isArray(data.response.result)) {
                        $scope.storesave = data.response.result[0];
                        $activityIndicator.stopAnimating();
                        toastr.info('Store data loaded', 'Information');
                    }
                }
            });
        }

        function errorCallback() {
            $window.alert("hi");
        }


        $scope.acsavestore = function() {
            if ($scope.isNew === true) {
                storeFactory.saveStore($scope.storesave).then(function(data) {
                    if (data.statusCode === 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.go('main.settingsStore');
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                });
            } else {
                storeFactory.updateStore($scope.storesave.store_id, $scope.storesave).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.go('main.settingsStore');
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                });
            }
        };
    });
