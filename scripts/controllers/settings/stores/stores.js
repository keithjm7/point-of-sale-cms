/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('ACStoresCTRL', function($scope, $state, $stateParams, $timeout, $activityIndicator, $filter, storeFactory, ngDialog, toastr, ngTableParams) {

        $scope.selectedDatas = [];

        $scope.init = function() {
            $scope.stores = [];
            $activityIndicator.startAnimating();
            /* jshint ignore:start */
            $scope.storeTable = new ngTableParams({
                page: 1,
                count: 10
            }, {
                getData: function($defer, params) {
                    storeFactory.getAllStore().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            if ($scope.toSearch) {
                                $scope.stores = $filter('filter')(data.response.result, $scope.toSearch);
                                params.total($scope.stores.length);
                                var responsedata = $scope.stores.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(responsedata);
                            } else {
                                $scope.stores = data.response.result;

                                params.total($scope.stores.length);
                                var responsedata = $scope.stores.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(responsedata);
                            }
                            $activityIndicator.stopAnimating();
                            toastr.info('List of stores successfully loaded', 'Information');
                        } else {
                            $activityIndicator.stopAnimating();
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }
            });
            /* jshint ignore:end */

        };


        $scope.changeSelectDeselectAll = function() {
            $scope.selectDeselectAll = !$scope.selectDeselectAll;
            _.each($scope.stores, function(product) {
                product.selected = $scope.selectDeselectAll;
                $scope.deleteSomeData = $scope.selectDeselectAll;
                $scope.checkDataChanges(product);
            });
        };

        $scope.checkDataChanges = function(contact) {
            if (contact.selected) {
                if (!_.find($scope.selectedDatas, function(selectedData) {
                        return selectedData.store_id === contact.store_id;
                    })) {
                    $scope.selectedDatas.push(contact);
                }
            } else {
                $scope.selectedDatas = _.filter($scope.selectedDatas, function(selectedData) {
                    return selectedData.store_id !== contact.store_id;
                });
            }
            $scope.deleteSomeData = ($scope.selectedDatas.length > 0) ? true : false;
        };

        $scope.refresh = function() {
            $scope.storeTable.reload();
        };

        $scope.deleteSelected = function() {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                var dataCountToDelete = 0;
                _.each($scope.selectedDatas, function(contact) {
                    if (contact.selected) {
                        ++dataCountToDelete;

                        storeFactory.deleteStore(contact.store_id, function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                toastr.success(data.response.msg, 'Success');
                                $state.reload();
                            } else {
                                toastr.error(data.response.msg, 'Error');
                                return;
                            }
                        });
                    }
                });
                $scope.refresh();
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };


        $scope.toggleModal = function(id, name, index) {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                $scope.removestore(id);
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.removestore = function(id) {
            storeFactory.deleteStore(id, function(data) {
                console.log('data: ', data);
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.refresh();
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        };

        $scope.edit = function(data) {
            console.log(data);
            $state.go('main.settingsAddStore', {
                data: data.store_id
            });
        };
    });
