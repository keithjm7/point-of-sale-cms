'use strict';

angular.module('pointofsale')
    .controller('categoriesCtrl', function(async, $scope, $state, $activityIndicator, $timeout, $filter, toastr, ngDialog, categoryFactory, ngTableParams, baseURL) {

        $scope.selectedDatas = [];

        $scope.init = function() {
            $scope.categories = [];

            $activityIndicator.startAnimating();
            /* jshint ignore:start */
            $scope.categoryTable = new ngTableParams({
                page: 1,
                count: 10
            }, {
                getData: function($defer, params) {
                    categoryFactory.getAllCategory().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.categories = data.response.result;
                            _.each($scope.categories, function(product) {
                                product.c_image = baseURL + product.c_image;
                            });

                            if ($scope.toSearch) {
                                $scope.categories = $filter('filter')($scope.categories, $scope.toSearch);
                                params.total($scope.categories.length);
                                var datas = $scope.categories.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(datas);
                            } else {
                                params.total($scope.categories.length);
                                var datas = $scope.categories.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(datas);
                            }
                            $activityIndicator.stopAnimating();
                            toastr.info('List of Categories successfully loaded', 'Information');
                        } else {
                            $activityIndicator.stopAnimating();
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }
            });
            /* jshint ignore:end */


        };

        $scope.refresh = function() {
            $scope.categoryTable.reload();
        };


        $scope.changeSelectDeselectAll = function() {
            $scope.selectDeselectAll = !$scope.selectDeselectAll;
            _.each($scope.categories, function(product) {
                product.selected = $scope.selectDeselectAll;

                $scope.deleteSomeData = $scope.selectDeselectAll;
                $scope.checkDataChanges(product);
            });
        };

        $scope.checkDataChanges = function(contact) {
            if (contact.selected) {
                if (!_.find($scope.selectedDatas, function(selectedData) {
                        return selectedData.id === contact.id;
                    })) {
                    $scope.selectedDatas.push(contact);
                }
            } else {
                $scope.selectedDatas = _.filter($scope.selectedDatas, function(selectedData) {
                    return selectedData.id !== contact.id;
                });
            }
            $scope.deleteSomeData = ($scope.selectedDatas.length > 0) ? true : false;
        };

        $scope.deleteSelected = function() {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                var dataCountToDelete = 0;
                _.each($scope.selectedDatas, function(contact) {
                    if (contact.selected) {
                        ++dataCountToDelete;
                        categoryFactory.deleteCategory(contact.id, function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                toastr.success(data.response.msg, 'Success');
                                $state.reload();
                            } else {
                                toastr.error(data.response.msg, 'Error');
                                return;
                            }
                        });
                    }
                });
                $scope.refresh();
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.toggleModal = function(id, index) {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                $scope.removeCategory(id);
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.removeCategory = function(id) {
            categoryFactory.deleteCategory(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.refresh();

                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        };

        $scope.edit = function(data) {
            $state.go('main.settingsCategory', {
                data: data.id
            });
        };
    });
