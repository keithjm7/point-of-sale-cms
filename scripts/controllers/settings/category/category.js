/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')


.controller('ACCatCTRL', function($scope, $state, $stateParams, categoryFactory, Upload, $http, API_URL, photoapi, $window) {

    $scope.treedata = [{
        "label": "User",
        "id": "role1",
        "children": [{
            "label": "subUser1",
            "id": "role11",
            "children": []
        }, {
            "label": "subUser2",
            "id": "role12",
            "children": [{
                "label": "subUser2-1",
                "id": "role121",
                "children": [{
                    "label": "subUser2-1-1",
                    "id": "role1211",
                    "children": []
                }, {
                    "label": "subUser2-1-2",
                    "id": "role1212",
                    "children": []
                }]
            }]
        }]
    }, {
        "label": "Admin",
        "id": "role2",
        "children": []
    }, {
        "label": "Guest",
        "id": "role3",
        "children": []
    }];


    function init() {
        $scope.stepsModel = [];
        $scope.stepsModel.push('img/default.png');
        var source = [];
        var children = [];
        $scope.Categsave = [];


        categoryFactory.getAllCategory().then(function(data) {

            $scope.temp = [];
            for (var i = 0; i < data.response.result.length; i++) {
                var item = data.response.result[i];



                // if(item["parent_id"] == null){
                //     $scope.temp.push({label: item.name, id: item.id, children: []});
                // }else{
                //    console.log(item.name, _.findIndex($scope.temp, function(o) { return o.id == item.parent_id; })) ;
                //    var index = _.findIndex($scope.temp, function(o) { return o.id == item.parent_id; });

                //    console.log($scope.temp[index].children);
                //    // temp.forEach(function(value){
                //    //      value.children.push({label: item.name, id: item.id, children: []});

                //    //      if(value.children.length >= 1){
                //    //          console.log('children : ', value.children.length);
                //    //      }
                //    // });
                // }


                var label = item["name"];
                var parentid = item["parent_id"];
                var id = item["id"];
                if (children[parentid]) {
                    item = {
                        parentid: parentid,
                        label: label,
                        // item: item
                        id: id
                    };
                    if (item != null) {
                        if (!children[parentid].children) {
                            children[parentid].children = [];
                        }
                        children[parentid].children[children[parentid].children.length] = item;
                        children[id] = item;
                    }
                } else {
                    children[id] = {
                        parentid: parentid,
                        label: label,
                        id: id
                    };
                    source[id] = children[id];
                }



            }

            var arr = [];
            var result = _(source).omit(_.isUndefined).omit(_.isNull).value();
            _.forEach(result, function(value, key) {
                console.log('VALUE', value);

                arr.push(value);
                console.log('Key', key);
            });

            console.log(arr);
            $scope.roleList = arr;
            console.log($scope.roleList);
        }).catch(function(err) {
            console.log(err);
        });
    }
    init();

    $scope.removeCat = function() {
        categoryFactory.deleteCategory($scope.todelete.TDid, function(data) {
            if (data.statusCode === 200) {
                $scope.showModal = false;
                init();
            }
        });
    };
    $scope.showModal = false;
    $scope.toggleModal = function(id, name) {
        $scope.showModal = !$scope.showModal;
        $scope.todelete = {
            TDid: id,
            TDname: name

        };
    };
    $scope.close = function() {
        $scope.showModal = false;
    };


    $scope.imageUpload = function(element) {
        var reader = new FileReader();
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(element.files[0]);
    };
    $scope.imageIsLoaded = function(e) {
        $scope.stepsModel.length = 0;
        $scope.$apply(function() {
            $scope.stepsModel.push(e.target.result);
        });
    };
    $scope.cancel = function() {
        init();
    };
    $scope.removethumb = function() {
        $scope.stepsModel[0] = 'img/default.png';
        $scope.Categsave.c_image = null;
    };
    $scope.forEdit = function(id) {
        $scope.stepsModel = [];
        categoryFactory.getCategory(id).then(function(data) {
            console.log(data.response[0].c_id);
            $scope.Categsave = data.response[0];
            if ($scope.Categsave.c_image === null && $scope.Categsave.c_image === undefined || $scope.Categsave.c_image === 'null') {
                $scope.stepsModel.push('img/default.png');
            } else {
                $scope.stepsModel.push(photoapi + data.response[0].c_image);
            }
            console.log($scope.Categsave);
        });
    };
    $scope.parentCatID = function(ID) {
        $scope.Categsave.cat_id = ID;
        console.log($scope.Categsave.cat_id);
    };
    $scope.acsaveCAT = function(parentID) {
        if ($scope.Categsave.c_image === 'img/default.png') {
            $scope.Categsave.c_image = null;
        } else {
            $scope.Categsave.c_image = $scope.Categsave.c_image;
        }
        if ($scope.noparentID === true) {
            $scope.Categsave.cat_id = '';
        }
        if ($scope.Categsave.c_id == null) {
            Upload.upload({
                url: API_URL + '/categories',
                fields: $scope.Categsave,
                c_image: $scope.Categsave.c_image
            }).then(function(resp) {
                console.log($scope.Categsave);
                console.log('Success ');
                init();
            }, function(resp) {
                console.log('Error status: ' + resp.status);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        } else {
            Upload.upload({
                url: API_URL + '/categories/' + $scope.Categsave.c_id,
                fields: $scope.Categsave,
                method: 'PUT',
                parent_id: $scope.Categsave.cat_id,
                c_image: $scope.Categsave.c_image
            }).then(function(resp) {
                console.log($scope.Categsave);
                console.log('Success ');
                init();
            }, function(resp) {
                console.log('Error status: ' + resp.status);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        }
    };



    $scope.Categsave = {};
    $scope.onSubmit = function(valid) {

        if (valid) {
            console.log("I'm Submitted");
            console.log($scope.Categsave);

        } else {

            console.log("Invalid Form");

        }

    };

    $scope.onClickShowHideIVHDropdown1 = function(e) {
        if (e.target.classList[0] === "dropDown" && e.target.parentElement.children.length === 2 && e.target.parentElement.children[1].classList[0] === "dropDownContent") {
            e.target.parentElement.children[1].classList.toggle("show");
        } else if (e.target.classList[0] === "dropDownButton" && e.target.parentElement.parentElement.children.length === 2 && e.target.parentElement.parentElement.children[1].classList[0] === "dropDownContent") {
            e.target.parentElement.parentElement.children[1].classList.toggle("show");
        } else if (e.target.classList[0] === "dropDownButtonIcon" && e.target.parentElement.parentElement.parentElement.children.length === 2 && e.target.parentElement.parentElement.parentElement.children[1].classList[0] === "dropDownContent") {
            e.target.parentElement.parentElement.parentElement.children[1].classList.toggle("show");
        }
        var selectedItm = getSelectedFormIVHTreeModel1($scope.stuff);
        console.log(selectedItm);
    };


    var getSelectedFormIVHTreeModel1 = function(ivhModel) {
        var selectedItems = [];
        angular.forEach(ivhModel, function(value) {
            if (value.selected) {
                selectedItems.push(value.label);
            }
            if (angular.isDefined(value.children) && value.children.length > 0) {
                angular.forEach(getSelectedFormIVHTreeModel1(value.children), function(vle) {
                    selectedItems.push(vle);
                });
            }
        });
        return selectedItems;
    };

    // $scope.collection = [{
    //     id: 1,
    //     name: 'item1',
    //     childrens: [{
    //             id: 2,
    //             name: 'item1_1'
    //         }, {
    //             id: 3,
    //             name: 'item2_2'
    //         }

    //     ]

    // }, {
    //     id: 4,
    //     name: 'item2',
    //     childrens: [{
    //         id: 5,
    //         name: 'item2_1'
    //     }, {
    //         id: 6,
    //         name: 'item2_2',
    //         childrens: [{
    //             id: 7,
    //             name: 'item2_2_1'
    //         }, {
    //             id: 8,
    //             name: 'item2_2_2'
    //         }]
    //     }]
    // }];

    // $scope.changeItem = function(value) {
    //     $scope.selectedItem = value;
    // };

    //         it('should check ng-options', function() {
    //   expect(element(by.binding('{selected_color:myColor}')).getText()).toMatch('red');
    //   element.all(by.model('myColor')).first().click();
    //   element.all(by.css('select[ng-model="myColor"] option')).first().click();
    //   expect(element(by.binding('{selected_color:myColor}')).getText()).toMatch('black');
    //   element(by.css('.nullable select[ng-model="myColor"]')).click();
    //   element.all(by.css('.nullable select[ng-model="myColor"] option')).first().click();
    //   expect(element(by.binding('{selected_color:myColor}')).getText()).toMatch('null');
    // });


    $scope.sample = function(id) {
        console.log(id);
    };

});
