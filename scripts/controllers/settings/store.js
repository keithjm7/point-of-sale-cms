/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('ACStoreCTRL', function($scope, $state, $stateParams, acSTOREfctry, Pagination) {
        $scope.storeData = [];
        $scope.store = {};
        if ($stateParams.data === null && $stateParams.data === undefined) {
            $scope.storesave = {};
        } else {
            $scope.storesave = $stateParams.data;
        }
        acSTOREfctry.getAllStore().then(function(data) {
            $scope.store = data.response;
            console.log($scope.store);
            if ($scope.store.length > 0) {
                $scope.store.forEach(function(data) {
                    $scope.storeData.push({
                        address: data.address,
                        email_address: data.email_address,
                        isVat: data.isVat,
                        name: data.name,
                        owner_name: data.owner_name,
                        store_id: data.store_id,
                        telno: data.telno,
                        tin: data.tin
                    });

                });
            }
            $scope.pagination = Pagination.getNew(3);
            $scope.pagination.numPages = Math.ceil($scope.storeData.length / $scope.pagination.perPage);

        });

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.store, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };

        $scope.close = function() {
            $scope.showModal = false;
        };

        $scope.showModal = false;
        $scope.toggleModal = function(id, name, index) {
            $scope.showModal = !$scope.showModal;
            $scope.todelete = {
                TDid: id,
                TDname: name,
                TDindex: index
            };
        };

        $scope.removestore = function(id, index) {
            acSTOREfctry.deleteStore($scope.todelete.TDid, function(data) {
                if (data.statusCode === 200) {
                    $scope.showModal = false;
                    $scope.store.splice($scope.todelete.TDindex, 1);

                }
            });
        };

        $scope.edit = function(data) {
            $scope.storesave = data;
            $state.go('main.settingsAddStore', {
                data: data
            });
        };

        $scope.acsavestore = function() {

            if ($scope.storesave.store_id == null) {
                acSTOREfctry.saveStore($scope.storesave).then(function(data) {
                    if (data.statusCode === 200) {
                        // init();
                        $state.go('main.settingsStore');
                    }
                });
            } else {
                acSTOREfctry.updateStore($scope.storesave.store_id, $scope.storesave).then(function(data) {
                    // init();
                    $state.go('main.settingsStore');
                });
            }
        };
    });
