'use strict';

angular.module('pointofsale')
    .controller('appconfigCtrl', function($scope) {
        $scope.UIShow = 'store';

        $scope.breadcrumb = "Store";

        $scope.storeUI = function() {
            $scope.UIShow = 'store';
            $scope.breadcrumb = "Store";
        };

        $scope.bankUI = function() {
            $scope.UIShow = 'bank';
            $scope.breadcrumb = "Bank";
        };

        $scope.attributesUI = function() {
            $scope.UIShow = 'attributes';
            $scope.breadcrumb = "Attributes";
        };

        $scope.categoryUI = function() {
            $scope.UIShow = 'category';
            $scope.breadcrumb = "Category";
        };

        $scope.prodtypeUI = function() {
            $scope.UIShow = 'prodtype';
            $scope.breadcrumb = "Store";
        };

        $scope.storeDetailUI = function() {
            $scope.UIShow = 'storedetail';
            $scope.breadcrumb = "Store Detail";
        };

        $scope.attributeUI = function() {
            $scope.UIShow = 'attributedetail';
            $scope.breadcrumb = "Attribute Detail";
        };


    });
