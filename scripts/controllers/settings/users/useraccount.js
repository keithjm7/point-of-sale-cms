'use strict';

angular.module('pointofsale')
    .controller('useraccountCtrl', function(_, async, $scope, $uibModalInstance, toastr, useraccountsFctry, user, employeeFctry, usergroupsFctry) {

        $scope.inputType = 'password';

        $scope.users = {};
        $scope.users.valid_until = new Date();

        $scope.employees = [];
        $scope.usergroups = [];

        $scope.popup1 = {
            opened: false
        };


        async.waterfall([
            function(callback) {
                employeeFctry.getAllEmployees().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.employees = data.response.result;
                        callback(null, data);
                    } else {
                        callback(null, 'error');
                    }
                });
            },
            function(resp, callback) {
                usergroupsFctry.getAllUserGroups().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.usergroups = data.response.result;
                        callback(null, $scope.usergroups);
                    } else {
                        callback(null, 'error');
                    }
                });
            },
            function(resp, callback) {
                if (!_.isUndefined(user)) {
                    useraccountsFctry.getUserAccount(user).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            if (_.isArray(data.response.result)) {
                                $scope.users = data.response.result[0];

                                var rollin = $scope.users.role_id.split(',');
                                $scope.users.employee = _.find($scope.employees, {
                                    'emp_id': $scope.users.emp_id
                                });

                                $scope.users.role = _.map(rollin, function(grp) {
                                    return _.find($scope.usergroups, function(o){
                                        return o.role_id == grp;
                                    });
                                });

                                $scope.users.valid_until = new Date($scope.users.valid_until);
                            }
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }
            }
        ]);

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.hideShowPassword = function() {
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
            } else {
                $scope.inputType = 'password';
            }
        };

        $scope.generatePassword = function() {
            var data = useraccountsFctry.generatePasswordString(8);
            $scope.users.password = data;
        };

        $scope.setDefaultUName = function(employee) {
            $scope.users.username = (employee.lastname).toUpperCase();
        };

        $scope.saveUser = function() {
            var role = [];

            $scope.users.emp_id = $scope.users.employee.emp_id;
            if ($scope.users.role && $scope.users.role.length > 0) {
                _.each($scope.users.role, function(product) {
                    role.push(product.role_id);
                });
                $scope.users.role_id = role.join();
            } else {
                $scope.users.role_id = '';
            }

            if (!_.isUndefined(user)) {
                useraccountsFctry.updateUserAccount(user, $scope.users).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            } else {
                useraccountsFctry.saveUserAccount($scope.users).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancelUser = function() {
            $uibModalInstance.dismiss('cancel');
        };
    });
