'use strict';

angular.module('pointofsale')
    .controller('usergroupCtrl', function(_, $scope, $uibModalInstance, userGroup, toastr, usergroupsFctry) {
        console.log('usergroupCtrl');

        if (!_.isUndefined(userGroup)) {
            usergroupsFctry.getUserGroupById(userGroup).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    if (_.isArray(data.response.result)) {
                        $scope.usergroup = data.response.result[0];
                        console.log('$scope.usergroup: ', $scope.usergroup);
                    }
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }

        $scope.saveUserGroup = function() {
            if (!_.isUndefined(userGroup)) {
                usergroupsFctry.updateUserGroup(userGroup, $scope.usergroup).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            } else {
                usergroupsFctry.createUserGroup($scope.usergroup).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancelUserGroup = function() {
            $uibModalInstance.dismiss('cancel');
        };
    });