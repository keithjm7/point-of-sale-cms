'use strict';
   /* jshint ignore:start */
angular.module('pointofsale')
    .controller('useraccountsCtrl', function(_, async, $scope, $state, $activityIndicator, $uibModal, $timeout, toastr, ngTableParams, ngDialog, useraccountsFctry, usergroupsFctry) {
     
        $scope.users = [];
        $scope.usergroups = [];
        $scope.selectDeselectAll = false;
        $scope.selectDeselectAll2 = false;

        $scope.newUserAccount = function(userID) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'templates/settings/useraccounts/usersmodal.html',
                controller: 'useraccountCtrl',
                backdrop: 'static',
                size: 'modal-md',
                resolve: {
                    user: function() {
                        return userID;
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.init();
            }, function() {
                console.info('Modal dismissed');
            });
        };

        $scope.newUserGroup = function(groupID) {
            console.log('groupID: ', groupID);
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'templates/settings/useraccounts/usergroupsmodal.html',
                controller: 'usergroupCtrl',
                backdrop: 'static',
                size: 'modal-md',
                resolve: {
                    userGroup: function() {
                        return groupID;
                    }
                }
            });

            modalInstance.result.then(function(response) {
                console.log('response: ', response);
                if (response === 'save') {
                    $scope.init();
                }
            }, function() {
                console.info('Modal dismissed');
            });
        };

        $scope.toggleModal = function(id, index, params) {
            console.log(params);
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                if (params === 'users') {
                    $scope.removeUser(id);
                } else {
                    $scope.removeUserGroup(id);
                }
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.removeUserGroup = function(id) {
            console.log('id: ', id);
            usergroupsFctry.deleteUserGroup(id).then(function(data) {
                console.log('data: ', data);
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.init();
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });

        };

        $scope.removeUser = function(id) {
            useraccountsFctry.deleteUserAccount(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.init();
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        };

        $scope.refreshUser = function() {
            $scope.users = [];
            $activityIndicator.startAnimating();
            useraccountsFctry.getAllUserAccounts().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.users = data.response.result;

                    $scope.accountsTable = new ngTableParams({

                        page: 1,
                        count: 3
                    }, {
                        total: $scope.users.length,
                        getData: function($defer, params) {
                            // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                            $scope.data = $scope.users.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
                    toastr.info('List of Users successfully loaded', 'Information');
                    $activityIndicator.stopAnimating();
                } else {
                    toastr.error(data.response.msg, 'Error');
                    $activityIndicator.stopAnimating();
                }

            });
        };

        $scope.refreshUserGroup = function() {
            $scope.usergroups = [];
            $activityIndicator.startAnimating();
            usergroupsFctry.getAllUserGroups().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    console.log(' data.response.result: ', data.response.result);
                    $scope.usergroups = data.response.result;

                    $scope.agroupsTable = new ngTableParams({

                        page: 1,
                        count: 10
                    }, {
                        total: $scope.usergroups.length,
                        getData: function($defer, params) {
                            // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                            $scope.data2 = $scope.usergroups.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
                    toastr.info('List of User Groups successfully loaded', 'Information');
                    $activityIndicator.stopAnimating();
                } else {
                    toastr.error(data.response.msg, 'Error');
                    $activityIndicator.stopAnimating();
                }

            });
        };

        $scope.setPermission = function(row) {
            $state.go('main.rolepermissions', {
                roleID: JSON.stringify(row)
            });
        };

        $scope.init = function() {
            $activityIndicator.startAnimating();

            async.waterfall([
                function(callback) {
                    usergroupsFctry.getAllUserGroups().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.usergroups = data.response.result;

                            $scope.agroupsTable = new ngTableParams({

                                page: 1,
                                count: 10
                            }, {
                                total: $scope.usergroups.length,
                                getData: function($defer, params) {
                                    // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                                    $scope.data2 = $scope.usergroups.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                    $defer.resolve($scope.data);
                                }
                            });
                            toastr.info('List of User Groups successfully loaded', 'Information');
                            callback(null, $scope.usergroups);
                        } else {
                            toastr.error(data.response.msg, 'Error');
                            $activityIndicator.stopAnimating();
                            callback(null, 'error');
                        }

                    });
                },
                function(resp, callback) {
                    useraccountsFctry.getAllUserAccounts().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.users = data.response.result;

                            _.each($scope.users, function(product) {
                                if (product.role_id) {
                                    product.userRoles = _.map((product.role_id).split(','), function(grp) {
                                        return _.find($scope.usergroups, function(o) {
                                            return o.role_id == grp;
                                        });
                                    });
                                }
                            });

                            console.log('$scope.users: ', $scope.users);

                            $scope.accountsTable = new ngTableParams({

                                page: 1,
                                count: 3
                            }, {
                                total: $scope.users.length,
                                getData: function($defer, params) {
                                    // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                                    $scope.data = $scope.users.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                    $defer.resolve($scope.data);
                                }
                            });
                            $activityIndicator.stopAnimating();
                            toastr.info('List of Users successfully loaded', 'Information');
                            callback(null, $scope.users);
                        } else {
                            toastr.error(data.response.msg, 'Error');
                            $activityIndicator.stopAnimating();
                            callback(null, 'error');
                        }

                    });
                }
            ]);
        };

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.users, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };


        $scope.showUserRoles = function(role) {
            $scope.useraccountRoles = _.map($scope.usergroups, function(grp) {
                return grp.role_id == role;
            });
        };
    });
/* jshint ignore:end */
