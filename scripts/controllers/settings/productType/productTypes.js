/*jslint eqeqeq:false*/

'use strict';
angular.module('pointofsale')
    .controller('ACPTypesCTRL', function($scope, $state, $stateParams, $timeout, $activityIndicator, ptypeFactory, Upload, toastr, ngTableParams) {

        $activityIndicator.startAnimating();
        ptypeFactory.getAllProdType().then(function(data) {
            if (data.statusCode === 200 && data.response.success) {
                $scope.PType = data.response.result;
                   /* jshint ignore:start */
                $scope.ptypeTable = new ngTableParams({
                    page: 1,
                    count: 3
                }, {
                    total: $scope.PType.length,
                    getData: function($defer, params) {
                        // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                        $scope.data = $scope.PType.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.data);
                    }
                });
                    /* jshint ignore:end */
                $activityIndicator.stopAnimating();
                toastr.info('List of Product Types successfully loaded', 'Information');
            } else {
                toastr.error(data.response.msg, 'Error');
            }

        });


        $scope.changeSelectDeselectAll = function() {
            _.each($scope.PType, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };

        $scope.removePType = function() {
            console.log($scope.todelete.TDid);
            ptypeFactory.deleteProdType($scope.todelete.TDid, function(data) {
                if (data.statusCode === 200) {
                    $scope.showModal = false;
                    $scope.PType.splice($scope.todelete.TDindex, 1);
                }
            });
        };

        $scope.edit = function(data) {
            $scope.ptypeSave = data;
            $state.go('main.settingsAddProducttype', {
                data: data.ptype_id
            });

        };

        $scope.showModal = false;
        $scope.toggleModal = function(id, name, index) {
            $scope.showModal = !$scope.showModal;
            $scope.todelete = {
                TDid: id,
                TDname: name,
                TDindex: index
            };
        };

    });
