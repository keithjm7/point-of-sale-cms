/*jslint eqeqeq:false*/

'use strict';
angular.module('pointofsale')
    .controller('ACPTypeCTRL', function($scope, $state, $stateParams, $timeout, $activityIndicator, acPTypefctry, Upload, $http, toastr, API_URL, photoapi) {

        $scope.PType = {};
        $scope.prodTypdeData = [];
        /* jshint ignore:start */
        if (_.isEmpty($stateParams.data) || _.isUndefined($stateParams.data)) {
            $scope.isNew = true;
            $scope.ptypeSave = {};
            $scope.ptypeSave.ptype_image = 'img/default.png';
        } else {
            $scope.isNew = false;

            console.log($stateParams.data);
            acPTypefctry.getProdType($stateParams.data).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    if (data.response && _.isArray(data.response.result)) {
                        $scope.ptypeSave = data.response.result[0];
                        console.log($scope.ptypeSave);
                        $activityIndicator.stopAnimating();
                        toastr.info('Store data loaded', 'Information');
                    }
                }
               console.log($scope.ptypeSave.ptype_image);
            if ( $scope.ptypeSave.ptype_image==null || $scope.ptypeSave.ptype_image==undefined) {
                
                $scope.ptypeSave.ptype_image = 'img/default.png';
                console.log("Grbe ka");
            } else {
                // $scope.origImage = $stateParams.data.ptype_image;
                $scope.ptypeSave.ptype_image = photoapi + $scope.ptypeSave.ptype_image;
                console.log("mali ang code");
            }
            });
             
        }

        // acPTypefctry.getAllProdType().then(function(data) {
        //     $scope.PType = data.response;
        //     console.log(data.response);
        //     $scope.PType.forEach(function(data) {
        //         $scope.prodTypdeData.push({
        //             description: data.description,
        //             label: data.label,
        //             name: data.name,
        //             ptype_id: data.ptype_id,
        //             ptype_image: data.ptype_image

        //         });
        //     });


        // });

        /* jshint ignore:end */

        $scope.acsaveProdType = function() {
            if ($scope.ptypeSave.ptype_image === 'img/default.png') {
                $scope.ptypeSave.ptype_image = null;
            } else {
                $scope.ptypeSave.ptype_image = $scope.ptypeSave.ptype_image;
            }



            if ($scope.ptypeSave.ptype_id == null) {
                // acPTypefctry.saveProdType($scope.ptypeSave).then(function(data) {
                //     if (data.statusCode == 200) {
                //         init();
                //         $state.go('main.settingsProducttype')
                //     }
                Upload.upload({
                    url: API_URL + '/productType',
                    fields: $scope.ptypeSave,
                    method: 'POST',
                    ptype_image: $scope.ptypeSave.ptype_image
                }).then(function(resp) {

                    console.log($scope.ptypeSave);
                    toastr.success(resp.msg, 'Success');
                    $timeout(function() {
                        $state.go('main.settingsProducttype');
                    }, 300);
                    console.log('Success ');

                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    // console.log('progress: ' + progressPercentage);

                });

            } else {
                console.log($scope.ptypeSave);

                if ($scope.ptypeSave.ptype_image === photoapi + $scope.origImage) {
                    $scope.ptypeSave.ptype_image = $scope.origImage;
                }
                Upload.upload({
                    url: API_URL + '/productType/' + $scope.ptypeSave.ptype_id,
                    fields: $scope.ptypeSave,
                    method: 'PUT',
                    ptype_image: $scope.ptypeSave.ptype_image
                }).then(function(resp) {
                    console.log($scope.ptypeSave);
                    toastr.success(resp.msg, 'Success');
                    $timeout(function() {
                        $state.go('main.settingsProducttype');
                    }, 300);
                    console.log('Success ');
                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    // console.log('progress: ' + progressPercentage);

                });
            }
        };

        $scope.removethumb = function() {

            $scope.ptypeSave.ptype_image = 'img/default.png';
        };
        $scope.close = function() {
            $scope.showModal = false;
        };

        $scope.uploadPic = function(file) {
            file.upload = Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                data: {
                    file: file,
                    username: $scope.username
                },
            });
            file.upload.then(function(response) {
                $timeout(function() {
                    file.result = response.data;
                });
            }, function(response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
            }, function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };
    });
