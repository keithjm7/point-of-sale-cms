'use strict';

angular.module('pointofsale')
    .controller('settingsAttribute', function($scope, $window, toastr, $state, $activityIndicator, attributeFactory, Upload, API_URL) {

        $scope.attrib = {};
        $scope.attrib.Attvalue = [{
            value: '',
            sku: ''
        }];
        $scope.attrib.Attvalue.valImage = [{
            value_image: ''
        }];
        $scope.attrib.is_visible = false;
        $scope.attrib.is_search = false;
        $scope.attrib.is_variation = false;
        $scope.attrib.is_custumizable = false;
        $scope.attrib.is_unique = false;

        $scope.addValue = function() {
            $scope.attrib.Attvalue.push({
                value: '',
                sku: ''
            });
            $scope.attrib.Attvalue.valImage.push({
                value_image: ''
            });
        };

        $scope.imageUpload = function(element) {
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(element.files[0]);
        };
        $scope.imageIsLoaded = function(e) {
            $scope.stepsModel.length = 0;
            $scope.$apply(function() {
                $scope.stepsModel.push(e.target.result);
            });
        };
        $scope.cancel = function() {
            // init();
        };
        $scope.removethumb = function() {
            $scope.stepsModel[0] = 'img/default.png';
            $scope.Categsave.c_image = null;
        };


        $scope.saveData = function() {
            ($scope.attrib.is_visible ? $scope.attrib.is_visible = 1 : $scope.attrib.is_visible = 0);
            ($scope.attrib.is_search ? $scope.attrib.is_search = 1 : $scope.attrib.is_search = 0);
            ($scope.attrib.is_variation ? $scope.attrib.is_variation = 1 : $scope.attrib.is_variation = 0);
            ($scope.attrib.is_custumizable ? $scope.attrib.is_custumizable = 1 : $scope.attrib.is_custumizable = 0);
            ($scope.attrib.is_unique ? $scope.attrib.is_unique = 1 : $scope.attrib.is_unique = 0);
            $scope.attrib.is_required = 0;
            attributeFactory.saveAttrib($scope.attrib).then(function(data) {
                console.log(data);
                $window.alert('saved');
                if (data.statusCode === 200) {

                    $state.go('main.settingsAttribute');
                }
            });

            ($scope.attrib.Attvalue.valImage === 'img/default.png' ? $scope.attrib.Attvalue.valImage = null : $scope.attrib.Attvalue.valImage = $scope.attrib.Attvalue.valImage);

            if ($scope.attrib.att_id === null) {
                Upload.upload({
                    url: API_URL + '/categories',
                    fields: $scope.Categsave,
                    c_image: $scope.Categsave.c_image
                }).then(function(resp) {
                    console.log($scope.Categsave);
                    console.log('Success ');
                    // init();
                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                });
            } else {
                Upload.upload({
                    url: API_URL + '/categories/' + $scope.Categsave.c_id,
                    fields: $scope.Categsave,
                    method: 'PUT',
                    c_image: $scope.Categsave.c_image
                }).then(function(resp) {
                    console.log($scope.Categsave);
                    console.log('Success ');
                    // init();
                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                });
            }


        };
    });
