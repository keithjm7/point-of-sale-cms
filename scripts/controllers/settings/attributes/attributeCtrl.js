'use strict';

angular.module('pointofsale')
    .controller('settingsAttributes', function($scope, $window, toastr, $state, $activityIndicator, attributeFactory, Upload, API_URL, ngTableParams) {

        $scope.stepsModel = [];
        $scope.stepsModel.push('img/default.png');


        $scope.init = function() {
            $scope.attribList = [];
            $activityIndicator.startAnimating();

            /* jshint ignore:start */
            $scope.attribTable = new ngTableParams({
                page: 1,
                count: 3
            }, {
                getData: function($defer, params) {
                    attributeFactory.getAllAttribute().then(function(data) {
                        console.log('data: ', data);
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.attribList = data.response.result;

                            // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                            params.total(data.length);
                            var responsedata = $scope.attribList.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve(responsedata);

                            $activityIndicator.stopAnimating();
                            toastr.info('List of Attributes successfully loaded', 'Information');
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }
            });
            /* jshint ignore:end */

        };

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.attribList, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };


        $scope.deleteData = function(id, index) {
            attributeFactory.deleteAttribute(id).then(function(data) {
                if (data.statusCode === 200) {
                    $scope.attribList.splice(index, 1);
                }
            }).catch(function(err) {
                console.log(err);
                toastr.error(err.statusText + " - Can't delete that item.", {
                    timeOut: 3000
                });
            });
        };
    });
