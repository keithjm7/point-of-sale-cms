 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('mainPageCtrl', function($scope, $location) {
         $scope.getClass = function(path) {
             if ($location.path().substr(0, path.length) === path) {
                 return 'active';
             } else {
                 return '';
             }
         };
     })
     .controller('MasterCtrl', function($scope, $cookieStore) {
         var mobileView = 992;

         $scope.getWidth = function() {
             return window.innerWidth;
         };

         $scope.$watch($scope.getWidth, function(newValue, oldValue) {
             if (newValue >= mobileView) {
                 if (angular.isDefined($cookieStore.get('toggle'))) {
                     $scope.toggle = !$cookieStore.get('toggle') ? false : true;
                 } else {
                     $scope.toggle = true;
                 }
             } else {
                 $scope.toggle = false;
             }

         });

         $scope.toggleSidebar = function() {
             $scope.toggle = !$scope.toggle;
             $cookieStore.put('toggle', $scope.toggle);
         };

         window.onresize = function() {
             $scope.$apply();
         };
     });
