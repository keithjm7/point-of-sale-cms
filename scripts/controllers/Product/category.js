 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('categoryCTRL', function($scope, $rootScope, $timeout, $state, toastr, categoryFactory) {

         $scope.categ = {
             repeatSelect: null,
             availableOptions: [],
         };

         $scope.store = {};

         categoryFactory.getAllCategory().then(function(data) {
             if (data.statusCode == 200 && data.response.success) {
                 $scope.categ.availableOptions = data.response.result;
             } else {
                 toastr.error(data.response.msg, 'Error');
             }
         });


         $scope.getData = function(ID) {
             categoryFactory.getCategory(ID).then(function(data) {
                 console.log('data: ',data);
             });
         };

         $scope.selectechanged = function() {
             $scope.selectedItem = $scope.itemArray[$scope.itemArray.id];
         };

         $scope.saveCategory = function() {
             if ($scope.category._id == null) {
                 categoryFactory.saveCategory($scope.category, function(data) {
                     if (data.statusCode === 200 && data.response.success) {
                         toastr.success(data.response.msg, 'Success');
                         $timeout(function() {
                             $state.go('main.viewProduct');
                         }, 300);
                     } else {
                         toastr.error(data.response.msg, 'Error');
                     }
                 });
             } else {
                 categoryFactory.updateCategory($scope.category._id, $scope.category, function(data) {
                     if (data.statusCode === 200 && data.response.success) {
                         toastr.success(data.response.msg, 'Success');
                         $timeout(function() {
                             $state.go('main.viewProduct');
                         }, 300);
                     } else {
                         toastr.error(data.response.msg, 'Error');
                     }
                 });
             }
         };




     });
