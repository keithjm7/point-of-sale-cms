'use strict';
angular.module('pointofsale')
    .controller('productCatalogueCtrl', function(_, $scope, $rootScope, $activityIndicator, $state, toastr, productFactory, categoryFactory, photoapi, Pagination) {

        $scope.selectedArray = [];
        $scope.productsData = [];
        $scope.categoryData = [];
        $scope.modulo = 0;
        $scope.notSelect = 1;
        $scope.selectDeselectAll = false;
        $scope.productShow = 'list';
        $scope.productDataView = 'viewAllProduct';
        var source = [];
        var items = [];

        categoryFactory.getAllCategory().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var categories = data.response.result;

                for (var i = 0; i < categories.length; i++) {
                    var item = categories[i];

                    var label = item['name'];
                    var parentid = item['parent_id'];
                    var id = item['id'];
                    if (items[parentid]) {
                        item = {
                            parentid: parentid,
                            label: label,
                            item: item
                        };
                        if (item != null) {
                            if (!items[parentid].items) {
                                items[parentid].items = [];
                            }
                            items[parentid].items[items[parentid].items.length] = item;
                            items[id] = item;
                        }
                    } else {
                        items[id] = {
                            parentid: parentid,
                            label: label,
                            item: item
                        };
                        source[id] = items[id];
                    }
                }
                $scope.categoryData = _(source).omit(_.isUndefined).omit(_.isNull).value();
            }
        });

        $activityIndicator.startAnimating();
        productFactory.getAllProduct().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $scope.productArray = data.response.result;
                if ($scope.productArray.length > 0) {
                    $scope.productArray.forEach(function(data) {
                        $scope.productsData.push({
                            is_published: data.is_published,
                            p_id: data.p_id,
                            item_name: data.item_name,
                            name: data.name,
                            product_imgPath: photoapi + data.product_imgPath,
                            sell_price: data.sell_price,
                            start_date: data.start_date
                        });
                    });
                    $activityIndicator.stopAnimating();
                    toastr.info('List of stores successfully loaded', 'Information');
                } else {
                    $activityIndicator.stopAnimating();
                }
            } else {
                toastr.error(data.response.msg, 'Error');
            }

            $scope.pagination = Pagination.getNew(12);
            $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);
        });

        $scope.changeSelectDeselectAll = function() {
            if ($scope.selectDeselectAll === true) {

                _.each($scope.productsData, function(product) {
                    product.selected = $scope.selectDeselectAll;
                    $rootScope.activeArchive.push(product.p_id);
                    $rootScope.publishUnpublish.push(product.p_id);
                });

            } else {
                _.each($scope.productsData, function(product) {
                    product.selected = $scope.selectDeselectAll;
                });
                $rootScope.activeArchive = [];
                $rootScope.publishUnpublish = [];
                console.log("publishUnpublish", $rootScope.publishUnpublish);
                console.log("activeArchive", $rootScope.activeArchive);
            }
        };


        $scope.selectRow = function(id) {

            // console.log("id",id);

            console.log("indexOf(id)", $scope.selectedArray.indexOf(id));

            if ($scope.selectedArray.indexOf(id) == -1) {
                $scope.selectedArray.push(id);
                // console.log("Bag-o ni dri");

            } else {
                $scope.selectedArray.splice($scope.selectedArray.indexOf(id), 1);
            }

        };


        $scope.isSelected = function(id) {
            // console.log("id", id);
            if ($scope.selectedArray.indexOf(id) > -1) {
                return true;
            }
            return false;
        };



        $scope.publish = function() {
            // $scope.selectedRow = null;

            console.log("$rootScope.publishUnpublish", $rootScope.publishUnpublish);
            console.log("First productDataView", $rootScope.productDataView);

            if ($rootScope.publishUnpublish.length > 0) {

                if ($rootScope.productDataView == 'viewPublishProduct') {
                    $scope.productsData = [];
                    $rootScope.notSelect = 1;
                    $rootScope.publishUnpublish = [];
                    $rootScope.activeArchive = [];
                    $scope.productsData = $rootScope.productsData;

                    console.log("$rootScope.productsData", $rootScope.productsData);

                    $scope.selectDeselectAll = false;
                    _.each($scope.productsData, function(product) {
                        product.selected = $scope.selectDeselectAll;
                        // $rootScope.activeArchive.push(product.p_id);
                        // $rootScope.publishUnpublish.push(product.p_id);
                    });


                } else {


                    productFactory.publishUnpublish(1, $rootScope.publishUnpublish).then(function(data) {
                        $rootScope.notSelect = 1;
                        $rootScope.publishUnpublish = [];
                        $rootScope.activeArchive = [];
                        $scope.productsData = [];
                        $scope.pagination = [];


                        if ($rootScope.productDataView == 'viewUnpublishProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getAllUnpublish().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });

                        }

                        if ($rootScope.productDataView == 'viewAllProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getAllProduct().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });

                        }


                        if ($rootScope.productDataView == 'viewSearchProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getSearch($rootScope.searchValue).then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }

                        if ($rootScope.productDataView == 'viewActiveProduct') {
                            console.log("Achive view", $rootScope.productDataView);
                            productFactory.getAllProduct().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }
                        if ($rootScope.productDataView == 'viewArchiveProduct') {
                            console.log("Achive view", $rootScope.productDataView);
                            productFactory.getAllArchived().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }





                    });

                }

            } else {

                $rootScope.productDataView = 'viewPublishProduct';
                $rootScope.notSelect = 1;
                $rootScope.activeArchive = [];
                $rootScope.publishUnpublish = [];
                $scope.productsData = [];
                $scope.pagination = [];
                $rootScope.modulo = 0;

                console.log("$rootScope.productDataView", $rootScope.productDataView);

                productFactory.getAllPublish().then(function(data) {
                    $scope.productArray = data.response.result;
                    $scope.productArray.forEach(function(data) {
                        $scope.productsData.push({
                            is_published: data.is_published,
                            p_id: data.p_id,
                            item_name: data.item_name,
                            name: data.name,
                            product_imgPath: photoapi + data.product_imgPath,
                            sell_price: data.sell_price,
                            start_date: data.start_date

                        });

                        $rootScope.productsData = $scope.productsData;

                    });
                    $scope.selectDeselectAll = false;
                    $scope.pagination = Pagination.getNew(12);
                    $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                });

            }

        };

        $scope.unpublish = function() {

            console.log("$rootScope.publishUnpublish", $rootScope.publishUnpublish);

            if ($rootScope.publishUnpublish.length > 0) {

                if ($rootScope.productDataView == 'viewUnpublishProduct') {
                    $scope.productsData = [];
                    $rootScope.notSelect = 1;
                    $rootScope.publishUnpublish = [];
                    $rootScope.activeArchive = [];
                    $scope.productsData = $rootScope.productsData;

                    console.log("$rootScope.productsData", $rootScope.productsData);

                    $scope.selectDeselectAll = false;
                    _.each($scope.productsData, function(product) {
                        product.selected = $scope.selectDeselectAll;
                        // $rootScope.activeArchive.push(product.p_id);
                        // $rootScope.publishUnpublish.push(product.p_id);
                    });


                } else {

                    productFactory.publishUnpublish(0, $rootScope.publishUnpublish).then(function(data) {
                        $rootScope.notSelect = 1;
                        $rootScope.publishUnpublish = [];
                        $rootScope.activeArchive = [];
                        $scope.productsData = [];
                        $scope.pagination = [];

                        if ($rootScope.productDataView == 'viewPublishProduct') {
                            console.log("viewPublishProduct", $rootScope.productDataView);
                            productFactory.getAllPublish().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });

                        }



                        if ($rootScope.productDataView == 'viewAllProduct') {


                            productFactory.getAllProduct().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);




                            });
                        }


                        if ($rootScope.productDataView == 'viewSearchProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getSearch($rootScope.searchValue).then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }

                        if ($rootScope.productDataView == 'viewActiveProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getAllProduct().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }
                        if ($rootScope.productDataView == 'viewArchiveProduct') {
                            console.log("viewAllProduct", $rootScope.productDataView);
                            productFactory.getAllArchived().then(function(data) {
                                $scope.productArray = data.response.result;

                                $scope.productArray.forEach(function(data) {
                                    $scope.productsData.push({
                                        is_published: data.is_published,
                                        p_id: data.p_id,
                                        item_name: data.item_name,
                                        name: data.name,
                                        product_imgPath: photoapi + data.product_imgPath,
                                        sell_price: data.sell_price,
                                        start_date: data.start_date

                                    });

                                    $rootScope.productsData = $scope.productsData;

                                });
                                $scope.selectDeselectAll = false;
                                $scope.pagination = Pagination.getNew(12);
                                $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                            });


                        }


                    });
                }
            } else {


                $rootScope.productDataView = 'viewUnpublishProduct';
                $rootScope.notSelect = 1;
                $rootScope.activeArchive = [];
                $scope.productsData = [];
                $scope.pagination = [];
                $rootScope.modulo = 0;

                productFactory.getAllUnpublish().then(function(data) {
                    $scope.productArray = data.response.result;
                    $scope.productArray.forEach(function(data) {
                        $scope.productsData.push({
                            is_published: data.is_published,
                            p_id: data.p_id,
                            item_name: data.item_name,
                            name: data.name,
                            product_imgPath: photoapi + data.product_imgPath,
                            sell_price: data.sell_price,
                            start_date: data.start_date

                        });

                        $rootScope.productsData = $scope.productsData;


                    });

                    $scope.selectDeselectAll = false;
                    $scope.pagination = Pagination.getNew(12);
                    $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);
                });

            }

        };



        $rootScope.activeArchive = [];
        $rootScope.publishUnpublish = [];
        $scope.activeArchive = function(p_id) {



            console.log("p_id", p_id);

            if ($rootScope.activeArchive.indexOf(p_id) == -1) {
                $rootScope.activeArchive.push(p_id);
                $rootScope.publishUnpublish.push(p_id);
                console.log("Archive and Restore", $rootScope.activeArchive);
                console.log("Publish and Unpublish", $rootScope.publishUnpublish);

            } else {
                $rootScope.activeArchive.splice($rootScope.activeArchive.indexOf(p_id), 1);
                $rootScope.publishUnpublish.splice($rootScope.publishUnpublish.indexOf(p_id), 1);
                console.log("Archive and Restore", $rootScope.activeArchive);
                console.log("Publish and Unpublish", $rootScope.publishUnpublish);
            }


            // console.log("$rootScope.activeArchive", $rootScope.activeArchive);
        };


        $scope.archived = function(oneOrZero) {



            if ($rootScope.activeArchive.length > 0) {



                // index < array.length
                // var temponeOrZero = {};

                if (oneOrZero == 1) {

                    console.log("oneOrZero", oneOrZero);
                    console.log("$rootScope.activeArchive", $rootScope.activeArchive);


                    productFactory.activeArchive(oneOrZero, $rootScope.activeArchive).then(function(data) {
                        $rootScope.activeArchive = [];

                        $scope.productsData = [];


                        productFactory.getAllProduct().then(function(data) {
                            $scope.productArray = data.response.result;

                            $scope.productArray.forEach(function(data) {
                                $scope.productsData.push({
                                    is_published: data.is_published,
                                    p_id: data.p_id,
                                    item_name: data.item_name,
                                    name: data.name,
                                    product_imgPath: photoapi + data.product_imgPath,
                                    sell_price: data.sell_price,
                                    start_date: data.start_date

                                });

                            });
                            $scope.selectDeselectAll = false;
                            $scope.pagination = Pagination.getNew(12);
                            $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);




                        });
                    });

                } else {


                    productFactory.activeArchive(oneOrZero, $rootScope.activeArchive).then(function(data) {
                        $rootScope.activeArchive = [];

                        $scope.productsData = [];

                        productFactory.getAllArchived().then(function(data) {
                            $scope.productArray = data.response.result;

                            $scope.productArray.forEach(function(data) {
                                $scope.productsData.push({
                                    is_published: data.is_published,
                                    p_id: data.p_id,
                                    item_name: data.item_name,
                                    name: data.name,
                                    product_imgPath: photoapi + data.product_imgPath,
                                    sell_price: data.sell_price,
                                    start_date: data.start_date


                                });
                            });


                            $scope.selectDeselectAll = false;
                            $scope.pagination = Pagination.getNew(12);
                            $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                        });
                    });


                }



            } else {

                console.log("$rootScope.notSelect", $rootScope.notSelect);
                if ($rootScope.notSelect == 1) {
                    $rootScope.productDataView = 'viewArchiveProduct';
                    $rootScope.notSelect = 0;
                    $scope.selectDeselectAll = false;

                    console.log("$rootScope.notSelect", $rootScope.notSelect);

                    $scope.productsData = [];
                    $scope.pagination = [];
                    productFactory.getAllArchived().then(function(data) {
                        $scope.productArray = data.response.result;
                        $scope.productArray.forEach(function(data) {
                            $scope.productsData.push({
                                is_published: data.is_published,
                                p_id: data.p_id,
                                item_name: data.item_name,
                                name: data.name,
                                product_imgPath: photoapi + data.product_imgPath,
                                sell_price: data.sell_price,
                                start_date: data.start_date

                            });

                        });
                        $scope.pagination = Pagination.getNew(12);
                        $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);
                    });

                } else {
                    $rootScope.productDataView = 'viewActiveProduct';
                    $rootScope.notSelect = 1;
                    $scope.selectDeselectAll = false;

                    console.log("$rootScope.notSelect", $rootScope.notSelect);

                    $scope.productsData = [];
                    $scope.pagination = [];
                    productFactory.getAllProduct().then(function(data) {
                        $scope.productArray = data.response.result;
                        $scope.productArray.forEach(function(data) {
                            $scope.productsData.push({
                                is_published: data.is_published,
                                p_id: data.p_id,
                                item_name: data.item_name,
                                name: data.name,
                                product_imgPath: photoapi + data.product_imgPath,
                                sell_price: data.sell_price,
                                start_date: data.start_date


                            });
                        });
                        $scope.pagination = Pagination.getNew(12);
                        $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);

                    });



                }
            }


        };


        $scope.search = function() {

            if ($scope.toSearch) {

                if ($rootScope.searchValue == $scope.toSearch) {
                    $scope.productsData = [];
                    $scope.productsData = $rootScope.productsData;
                } else {

                    $rootScope.productDataView = 'viewSearchProduct';
                    console.log("$rootScope.productDataView", $rootScope.productDataView);

                    $scope.productsData = [];
                    $scope.pagination = [];

                    $rootScope.searchValue = $scope.toSearch;


                    productFactory.getSearch($scope.toSearch).then(function(data) {

                        $scope.productArray = data.response.result;
                        $scope.productArray.forEach(function(data) {
                            $scope.productsData.push({
                                is_published: data.is_published,
                                p_id: data.p_id,
                                item_name: data.item_name,
                                name: data.name,
                                product_imgPath: photoapi + data.product_imgPath,
                                sell_price: data.sell_price,
                                start_date: data.start_date

                            });

                            $rootScope.productsData = $scope.productsData;

                        });

                        $scope.pagination = Pagination.getNew(12);
                        $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);
                    });

                }

                // $scope.pagination = Pagination.getNew(5);
                // $scope.pagination.numPages = Math.ceil($scope.productsData.length/$scope.pagination.perPage);   

            } else {
                $rootScope.productDataView = 'viewAllProduct';
                console.log("$rootScope.productDataView", $rootScope.productDataView);
                $scope.productsData = [];
                $scope.pagination = [];
                productFactory.getAllProduct().then(function(data) {
                    $scope.productArray = data.response.result;
                    $scope.productArray.forEach(function(data) {
                        $scope.productsData.push({
                            is_published: data.is_published,
                            p_id: data.p_id,
                            item_name: data.item_name,
                            name: data.name,
                            product_imgPath: photoapi + data.product_imgPath,
                            sell_price: data.sell_price,
                            start_date: data.start_date

                        });

                    });


                    console.log("productsData", $scope.productsData);
                    // console.log($scope.productList);
                    $scope.pagination = Pagination.getNew(12);
                    $scope.pagination.numPages = Math.ceil($scope.productsData.length / $scope.pagination.perPage);
                });
            }


        };

        $scope.changeUIlist = function() {
            if ($scope.productShow === 'grid') {
                $scope.productShow = 'list';
                console.log($scope.productShow);
            }
        };

        $scope.changeUIgrid = function() {
            if ($scope.productShow === 'list') {
                $scope.productShow = 'grid';
                console.log($scope.productShow);
            }
        };
    });
