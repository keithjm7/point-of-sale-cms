/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('storeCTRL', function(_, $scope, $state, $stateParams, $filter, async, toastr, storeFactory, priceHistoryFactory) {

        $scope.stores = [];
        $scope.storesCopy = [];
        $scope.prodStores = [];
        $scope.selectedStores = [];

        $scope.applicationStatus = '';

        async.waterfall([
            function(callback) {
                storeFactory.getAllStore().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.stores = data.response.result;
                        $scope.storesCopy = angular.copy($scope.stores);
                        callback(null, $scope.stores);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            },
            function(stores, callback) {
                if ($stateParams.product_id) {
                    priceHistoryFactory.getPriceHistory($stateParams.product_id).then(function(data) {
                        if (data.statusCode == 200 && data.response.success && data.response.result.length > 0) {
                            $scope.prodStore = data.response.result;

                            _.each($scope.storesCopy, function(store) {
                                var result = _.find($scope.prodStore, function(selectedstore) {
                                    return store.store_id === selectedstore.store_id;
                                });
                                if (result) {
                                    store.selected = true;
                                    store.base_price = result.base_price;
                                    store.regular_price = result.regular_price;
                                    store.rrp = result.rrp;
                                    store.sale_price = result.sale_price;
                                    store.sale_percent = result.sale_percent;
                                    store.fk_product_id = $stateParams.product_id;

                                    $scope.selectedStores.push(store);
                                }
                            });
                            $scope.itemArray = angular.copy($scope.storesCopy);

                        } else if (data.statusCode == 200 && data.response.success && data.response.result.length === 0) {
                            $scope.itemArray = angular.copy(stores);
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                } else {
                    $scope.itemArray = angular.copy(stores);
                }
            }
        ]);

        $scope.updateStatus = function(status) {
            $scope.applicationStatus = status;
            if (status === '') {
                $scope.itemArray = angular.copy($scope.storesCopy);
            } else {
                $scope.itemArray.splice(0, $scope.itemArray.length);

                _.each($scope.storesCopy, function(store) {
                    if (_.find($scope.selectedStores, function(selectedstore) {
                            return store.store_id === selectedstore.store_id;
                        })) {
                        $scope.itemArray.push(store);
                    }
                });
            }
        };

        $scope.searchStore = function() {
            if ($scope.search) {
                $scope.itemArray.splice(0, $scope.itemArray.length);
                var searchData = $filter('filter')($scope.storesCopy, $scope.search);
                $scope.itemArray = angular.copy(searchData);
            }
        };

        $scope.searchStatus = function() {
            if ($scope.search === '') {
                $scope.itemArray.splice(0, $scope.itemArray.length);
                $scope.itemArray = angular.copy($scope.storesCopy);
            }
        };

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.itemArray, function(data) {
                data.selected = $scope.selectDeselectAll;
                $scope.checkStoresChanges(data);
            });
        };


        $scope.checkStoresChanges = function(store) {
            if (store.selected) {
                var result = _.find($scope.selectedStores, function(selectedStore) {
                    return selectedStore.store_id === store.store_id;
                });

                if (!result) {
                    store.base_price = 0;
                    store.regular_price = 0;
                    store.rrp = 0;
                    store.sale_price = 0;
                    store.sale_percent = 0;
                    store.fk_product_id = $stateParams.product_id;

                    $scope.selectedStores.push(store);
                }
            } else {
                $scope.selectedStores = _.filter($scope.selectedStores, function(selectedStore) {
                    return selectedStore.store_id !== store.store_id;
                });
            }

        };

        function saveStore() {
            if ($stateParams.product_id) {
                priceHistoryFactory.saveSP($stateParams.product_id, $scope.selectedStores).then(function(data) {
                    if (data.statusCode === 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $state.go($state.current, {}, { reload: true });
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            } else {
                toastr.warning('WARNING: Unable to save.. no available products', 'WARNING');
                return;
            }
        }

        $scope.$on('saveStore', function(e) {
            saveStore();
        });



    });
